# Ref-Card-03
Die Ref-Card-03 ist eine Springboot-Applikation, die mit einer Datenbank verbunden ist. In dieser Datenbank sind verschiedene Witze eingetragen. Wenn die Applikation läuft wird eine Liste von Witzen age

## Requirements
- Es wird ein Windows/Linux/Mac Betriebssystem benötigt
- Es wird mindestens die Java-Version 11 auf dem Gerät installiert sein.

## Installation
- Eine Fork von Ref Crad 03 erstellen und auf den Computer klonen
- Docker itlab runner starten

## Im Projekt
### Dockerfile
- Maven Verison angeben:
```
FROM maven:3-openjdk-11-slim
```
- Die Namen der Umgebungsvariabeln in der RDS Datenbank angeben:
```
ENV DB_URL=""
ENV DB_USERNAME=""
ENV DB_PASSWORD=""
```
- Kopieren des Pfades der Applikation und des pom.xml:
```
COPY src /src
COPY pom.xml /
```
- Projekt kompimpilieren und jar-Datei holen:
```
RUN mvn -f pom.xml clean package
RUN mv /target/*.jar app.jar
```
- Umgebungsvariabeln beim Eintritt mitgeben und Applikation starten:
```
ENTRYPOINT ["java","-jar","/app.jar", "--DB_URI=${DB_URL}", "--DB_USER=${DB_USERNAME}", "--DB_PASS=${DB_PASSWORD}"]
```

### .gitlab.ci.yml
Dem Vorgegebenen Code das automatische Deploy hinzufügen:
```deploy:
    stage: deploy
    before_script: 
    - apk add --no-cache py3-pip 
    - pip install awscli 
    - aws --version
    script: 
    - aws ecs update-service --cluster $AWS_CLUSTER --service $AWS_SERVICE --force-new-deployment --region $AWS_DEFAULT_REGION
```

## In AWS
1. Repository erstellen
2. Erstes Image mit dem Tag latest erstellen und ins Repository pushen
3. RDS Datenbank erstellen danach direkt anzgezeigtes Passwort abspeichern
4. Cluster erstellen
5. Taskdefinition erstellen, worin die Umgebungsvariabeln aus dem Dockerfile mit dem Wert der RDS Datenbank angegeben werden.
6. Service erstellen
Bei Unklarheiten in der [Anleitung](https://kbwk-my.sharepoint.com/:w:/g/personal/evan_lueber_stud_kbw_ch/ER_QwzrZ2o9Jhu1L531OG-IB6zR0vVVYsT563cEHnIkLNA?e=IFwv5S) nachschauen

## In Gitlab
- Unter Settings unter CI/CD Die Variabeln hinzufügen:
    - AWS_ACCESS_KEY_ID
    - AWS_DEFAULT_REGION
    - AWS_SECRET_ACCESS_KEY
    - AWS_SESSION_TOKEN
    - CI_AWS_ECR_REGISTRY
    - CI_AWS_ECR_REPOSITORY_NAME
ACHTUNG: Die Variabeln AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY und AWS_SESSION_TOKEN müssen beim Starten des Learner Labs immer aktualisiert werden.